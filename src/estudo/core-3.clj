(ns estudo.core3)
; em modo geral , defn é união de def com fn [ def com uma função anonima]
; podemos escrever de duas formas
; def com fn
(def valor "Estatísticas - parte 1 ")
(def funcao (fn [episodio] (str "[DEF+FN] Episodio de hoje: " episodio)))
(println (funcao valor))
; defn
(defn funcao-defn [episodio]
  (str "[DEFN] Episodio de hoje: " episodio))
(println (funcao-defn valor))
; utilizando aridade com função anonima
(defn funcao-com-arity
([episodio] (fn [episodio] (str "[ARITY] Episodio de hoje: " episodio)))
([] funcao-com-arity "Seu Madruga sapateiro (1978) partes 1, 2 e 3")
)
(println (funcao-com-arity))
;utilizando aridade com println
(defn funcao-arity-defn
  ([episodio] (println "[println] Episodio de hoje: " episodio))
  ([] (funcao-arity-defn "Seu Madruga sapateiro (1978) partes 1, 2 e 3"))
)
(funcao-arity-defn)
; imprimindo varios episodios do chaves
(defn episodios [seriado & episodio] (str seriado episodio))
(println
  (episodios
    "Chaves" "Estatísticas - parte 1"
  "Seu Madruga sapateiro (1978) partes 1, 2 e 3"
  )
)

; syntax de função anonima
(def fa #(+ 1 %)) ; # vai representar uma função anonima e % é argumento
(println "Resultado para função anonima é: " (fa 2))

; função anonima com dois argumentos
(def fb #(* %1 %2))
(println "Resultado para função anonima fb: " (fb 5 6)))
