;RESUMO VETOR, SET, LIST E MAP
;SEJA V UM VETOR COM COM VALORES 1 2 3
(def V [1 2 3])
(assert (= (vector 1 2 3) V))
;PROVAR QUE PRIMEIRO ELEMENTO É IGUAL 1
(assert (= 1 (get V 0)))
;PROVAR QUE INDICE -1 RETORNA NIL
(assert (= nil (get V -1)))
;PROVAR QUE CONTAINS NÃO FUNCIONA EM VECTOR
(assert (= false (contains? V 3)))
(assert (= 3 (get V 2)))
;PROVAR QUE CONJ FUNCIONA NO TIPO vector
(assert (= [1 2 3] ( conj (vector 1 2) 3)))
; SEJA S UM SET DE VALORES SEM ORDEM E UNICOS
(def S #{1 2 3})
(assert (= #{1 2 3} S))
;SEJA SA, S COM VALOR 4
(def SA (conj S 4))
(assert (= #{1 2 3 4} SA))
;SEJA SB, SA SEM VALOR 1
(def SB (disj SA 1))
(assert (= #{2 3 4} SB))
;
;provando que em SB existe o elemento numerico igual 3
(assert (= (contains? SB 3)))
;
;SEJA L UMA LISTA COM VALORES 10 11 12 13
(def L (list 10 11 12 13))
(assert (= '(10 11 12 13) L))
;PROVAR QUE FUNÇÃO GET NAO FUNCIONA EM LIST
(assert (= nil (get L 0)))
(assert (= 10 (peek L)))
;
; SEJA M UM MAP COM NOME E IDADE
(def M {:nome "joy" :idade 39})
(assert (= {:nome "joy" :idade 39} M))
; PROVANDO QUE M CONTEM CHAVES :nome e :idade
(assert (= true (contains? M :nome) (contains? M :idade)))
; PROVANDO QUE M CONTEM OS VALORES "joy" e 39
(assert (= '("joy" 39) (vals M)))
;PROVANDO DE UMA FORMA MAIS VERBOSA
(assert (= "joy" (first (vals M) ) ) )
(assert (= 39 (first (rest (vals M)))))
; SEJA MA , ASSOCIAÇÃO DE M COM CHAVE VALOR
(def MA (assoc M :chave "valor"))
;PROVAR QUE :chave existe em MA e não existe em M
(assert (= true (contains? MA :chave)))
(assert (not= true (contains? M :chave)))
;SEJA MB, dessociar :idade de M
(def MB (dissoc M :idade))
;PROVAR QUE :idade não existe em MB
(assert (= false (contains? MB :idade)));
;provar que valor :idade de M é igual a 39
(assert (= 39 (get M :idade)))
;provar que valor :nome de M é igual a joy
(assert (= "joy" (get M :nome)))
;provar que M retorna nil quando uma chave não existe
(assert (= false (contains? M :endereco)))
(assert (= nil (get M :endereco)))
;
; UTILIZANDO O PROPRIO MAPA , RETORNAR :nome e :endereco
(assert (= 39 (M :idade)))
(assert (= "rua" (
          (assoc M :endereco "rua")
            :endereco)
      )
)
;PROVAR QUE CONJ DE UM VECTOR E LIST GERA UM VETOR COM LISTA INTERNA
;SEJA VE , VECTOR COM 0 1 2 COMO VALORES
(def VE (vector 0 1 2))
;SEJA LISTA , LI COM 3 4 5 COMO VALORES
(def LI (list 3 4 5))
;SEJA VALOR DE SAIDA
(def VS (vector 0 1 2 '(3 4 5)))
(assert (= VS (conj VE LI)))
;PROVAR QUE CONJ DE UMA LISTA COM VECTOR GERA UMA LISTA COM VECTOR INTERNO
(def VSL (list [0 1 2] 3 4 5))
(assert (= VSL (conj LI VE)))
;PROVAR QUE INTO CRIA UMA NOVA COLEÇÃO COM TIPO DA PRIMEIRA COLEÇÃO INSERIDA COMO ARGUMENTO
(assert (= (list 2 1 0 3 4 5) (into LI VE)))
; SEJA MAPA URL_FATURAMENTO :url = lb://servico-faturamento :headers outro mapa
(def URL_FATURAMENTO
  {
    :url "lb://servico-faturamento"
    :headers {
      :content-type "Application/JSON"
    }
})
;PROVAR QUE INTO CRIA UMA NOVA URL_FATURAMENTO COM :port
(def URL_FATURAMENTO_NOVO (into URL_FATURAMENTO {:port 80}))
(assert (= true
  (contains? URL_FATURAMENTO_NOVO :port)
  (contains? URL_FATURAMENTO_NOVO :url)
  (contains? URL_FATURAMENTO_NOVO :headers)
  (contains? (get URL_FATURAMENTO_NOVO :headers) :content-type)
))
