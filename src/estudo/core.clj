(ns estudo.core)

;; CRIANDO UMA FUNCAO
(defn funcao [x] (+ x 1))
(printf "%d" (funcao 2))


;; criando coleções literais
(list 1 2 3)
[1 2 3]
(set #{1 2 3})
(def y (vector 1 2 3))
(conj [1 2 3] y)

;; incrementando valor
(map inc (list 1 2 3))

;; FUNCAO
(defn a [x] x)
(defn funcao [x y] (+ x y))
(funcao (a 4) 2)


;; CLOJURE TEM COMO BASE  S -> SS ; S -> (S) ; S -> €
;; PORTANTO uma expressao clojure é sempre (funcao ....argumentos* )

;; CRIAR UMA FUNÇÃO QUE RETORNA UM DADO VALOR MULTIPLICADO POR 4
;; UTILIZAR A MESMA FUNCAO E SOMAR 10
(defn funcao-a [x] (* x 4))
(defn funcao-b [x] (+ (funcao-a x) 10))
(funcao-b 1)

;; IMPRIMINDO FUNCAO-B
(println "valor da função b :" (funcao-b 1))

; ( 7 + 3 * 4 + 5 ) / 10.
(defn funcao-matematica [] ( / (+ 7 (* 3 4) 5) 10))
(println "valor da função matematica: " (funcao-matematica))


;
(defn p [] (+ 1))
(println "rsultado : " (p))
;
