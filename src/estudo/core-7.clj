(ns estudo.core-7)
;  LIDANDO COM COLEÇÕES
; VALIDANDO IMUTABILIDADE
; SEJA UM VECTOR A com valores 1,2,3
(def A (vector 1 2 3))
; SEJA DEFINIÇÃO DE B QUE UNI UM VETOR COM VETOR A
(def B (conj A 4 5 6))
;PROVANDO QUE A É DIFERENTE DE B
(assert (not= A B))
; LOGO B NÃO ALTEROU O ESTADO DE A, MAS SIM CRIOU UM NOVO VECTOR
; retornando valor de index 1 do VECTOR
(assert (= 1 (get A 0) (get B 0)))
;VALIDANDO QUE VECTOR A POSSUI TAMANHO 3
(assert (= 3 (count A)))
;DEFININDO C COMO VETOR B MAIS VALOR 7
(def C (conj B 7))
(assert (= 7 (get C 6)))
;provando que obter valor de vetor fora do index retorna nil
(assert (= 1 (get C 0)))
(assert (= 7 (count C)))
(assert (= nil (get C 7)))
; CRIANDO UMA LISTA
(def lista (list 1 2 3))
;VALIDANDO QUE LISTA POSSUI 3 ITENS
(assert (= 3 (count lista)))
; INSERINDO VALOR 4 EM LISTA
(def lista-com-valor-4 (conj lista 4))
(assert (= '(4 1 2 3)  lista-com-valor-4))
; LISTA NÃO POSSUI INDEX, LOGO NÃO É POSSIVEL OBTER UM VALOR DE UMA POSICA EXPECIFICA
; PROVANDO QUE COMANDO FIST RETORNA PRIMEIRO VALOR DA LISTA
(assert (= 4 (first lista-com-valor-4)))
;PROVANDO QUE REST RETORNA TODOS VALORES EXCETO O PRIMEIRO
(assert (= '(1 2 3) (rest lista-com-valor-4) ))
;DEFINIR UMA FUNÇÃO QUE CRIA UMA LISTA COM VALORES IGUAL AO SENO X, SENDO X A ENTRADA
(defn lista-seno [x]
  (let [
      valor (Math/sin x)
    ]
    (list valor)
  )
)
(def piDivididoPor2 ( / Math/PI 2))
(assert (= '(1.0) (lista-seno piDivididoPor2)))
