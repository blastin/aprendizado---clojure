(ns estudo.core-8)
;provar que lista pode conter tipos de literais distintos;
; SEJA DEFINIÇÃO A: UMA LISTA COM VALORES 4 5 'A' :B
(def A (list 4 5 'A' :B))
;PROVAR QUE ESTES VALORES EXISTEM
(assert (= '(4 5 A' :B) A))
;VALIDAR QUE 4 É O PRIMEIRO VALOR
(assert (= 4 (first A)))
;VALIDAR QUE 4 'A' :B representa o resto da lista
(assert (= '(5 A' :B) (rest A)))
;PROVAR QUE AO ADICONAR UM ELEMENTO, ELE VIRA O PRIMEIRO DA LISTA E
; O PRIMEIRO ANTERIOR É O PRIMEIRO DO RESTO
(def B (conj A 9))
(assert (= 9 (first B)))
(assert (= 4 (first A) (first
                (rest B)
             )
        )
)
;TRABALHANDO COM STACK [PILHA]
;SEJA C UMA LISTA COM VALORES :dnb: :eletronica :house
(def C '(:dnb :eletronica :house))
;PROVAR QUE PEEK CAPTURA O PRIMEIRO DA PILHA
(assert (= :dnb (peek C)))
;PROVAR QUE POP REMOVE PRIMEIRO DA  PILHA E RETORNA O RESTO
(assert (= '(:eletronica :house) (pop C)))
;PROVAR QUE SEMANTICAMENTE REST E POP FUNCIONA DA MESMA FORMA
(assert (= '(:eletronica :house) (pop C) (rest C)))
;PROVA QUE C CONTINUA IMUTAVEL
(assert '(:dnb :eletronica :house) (C))
