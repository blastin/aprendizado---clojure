(ns estudo.core-5)

;let binds symbols to values in a "lexical scope".
; A lexical scope creates a new context for names,
; nested inside the surrounding context.
; Names defined in a let take precedence over the names in the outer context.
(defn funcao-a [x] (* x 32))
(assert (= 128 (funcao-a 4)))
(defn funcao-b [x] (+ x (funcao-a x) ))
(assert (= 198 (funcao-b 6)))

(defn funcao-c []
  (let [
    x (funcao-a 4)
    y (funcao-b 6)]
    (str x " " y)
  )
)
(println (funcao-c))
(assert (= "128 198" (funcao-c)))

;definição para uma função
; (defn [a b c &d] body)
; body -> chamadas de funções
;Define a function triplicate which takes another function and calls it three times,
; without any arguments.
(def funcao #(println "Hello World"))
(defn triplicate [f] (f) (f) (f))
(triplicate funcao)


(defn pessoa [nome idade]
  (let [
    nome nome
    idade 1
  ])
  (str "[ nome da pessoa = " nome " com idade = " idade "]")
)

(println (pessoa "Jefferson" 28))
