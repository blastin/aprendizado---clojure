(ns estudo.core-6)
(def booleano (boolean false))
(println booleano)
;
; Define a function opposite which takes a single argument f.
;It should return another function which takes any number of arguments,
;applies f on them, and then calls not on the result.
; The not function in Clojure does logical negation.
(defn indie [ & argumento] (str argumento))
(defn opposite [f]
  (fn [ & arg ] (not (apply f arg)))
)
((opposite indie) "oi")
;
; 10) Using the java.lang.Math class (Math/pow,
; Math/cos, Math/sin, Math/PI), demonstrate the following mathematical facts:
;
;     The cosine of pi is -1
;
;     For some x, sin(x)^2 + cos(x)^2 = 1
;
(defn funcao [] (Math/cos Math/PI))
(assert (= -1.0 (funcao)))
;
(defn demonstracao [x]
  (let [
    sin (Math/sin x)
    cos (Math/cos x)
    ]
    (+ (Math/pow sin 2) (Math/pow cos 2)))

)
(assert (= 1.0 (demonstracao Math/PI)))
;
; de forma mais simples
(defn demonstracao [x]
  ( +
    (Math/pow (Math/sin x) 2)
    (Math/pow (Math/cos x) 2)
  )
)
(assert (= 1.0 (demonstracao Math/PI)))
