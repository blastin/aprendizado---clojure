;CONTINUIDADE CONHECIMENTO EM
;SEJA DEFINIÇÃO DE PESSOA : COM NOME E IDADE
(def PESSOA {:nome "Senhor Madruga" :idade 48})
;PROVAR QUE PESSOA CONTEM NOME E IDADE
(assert (= true (contains? PESSOA :nome) (contains? PESSOA :idade)))
;PROVAR QUE IDADE DA PESSOA É EXATAMENTE IGUAL A 48
(assert (= 48 (get PESSOA :idade)))
;PROVAR QUE NOME DA PESSOA É Senhor Madruga
(assert (= "Senhor Madruga" (get PESSOA :nome)))
;PROVAR QUE :idade é diferente de "idade"
(assert (= 48 (get PESSOA :idade)))
(assert (not= 48 (get PESSOA "idade")))
;PROVAR POR FIM QUE "IDADE NÃO É MAPEADO EM PESSOA"
(assert (= nil (get PESSOA "idade")))
(assert (= false (contains? PESSOA "idade")))
;SEJA PESSOA_B, PESSOA COM CEP
(def PESSOA_B (assoc PESSOA :cep "02344684-4"))
;PROVAR QUE PESSOA_B CONTÉM CEP E QUE PESSOA NÃO FOI ALTERADO
(assert (= true (contains? PESSOA_B :cep)))
(assert (= {:nome "Senhor Madruga" :idade 48} PESSOA))
;SEJA PESSOA-C, PESSOA_B SEM nome
(def PESSOA-C (dissoc PESSOA_B :nome))
(assert (= false (contains? PESSOA-C :nome)))
;SEJA VALORES, TODOS VALORES DE PESSOA-C
(def VALORES (vals PESSOA-C))
(assert (= '(48 "02344684-4") VALORES))
;SEJA CHAVES, TODOAS CHAVES DE PESSOA-C
(def CHAVES (keys PESSOA-C))
(assert (= '(:idade :cep) CHAVES))
