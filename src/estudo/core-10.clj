(ns estudo.core-10)
; SEJA f(x) = ax² + bx + c
; CALCULAR X1 E X2
; DELTA = b² -4ac
; x1 = (-b + RAIZ DELTA) /2
; x2 = (-b - RAIZ DELTA) / 2
;SEJA RAIZ(X) = RAIZ X
(defn RAIZ [x] (Math/sqrt x))
(assert (= 4.0 (RAIZ 16)))
;
;SEJA D(A,B,C) = B² - 4ac
(defn DELTA [A B C] (- (Math/pow B 2) (* 4 A C)))
(assert (= 25.0 (DELTA 3 7 2)))
;
; SEJA CALCULO UMA FUNÇÃO QUE RETORNA RAIZ QUADRADA DE DELTA
(defn CALCULO [A B C] (RAIZ (DELTA A B C)))
(assert (= 5.0 (CALCULO 2 7 3)))
; SEJA F UMA FUNÇÃO QUE RECEBE UMA FUNÇÃO H
; CALCULA VALOR Xi : (-b +- RAIZ DELTA) / 2
(defn X [FUNCAO A B C] (/ (FUNCAO (- B) (CALCULO A B C)) 2))
(assert (= -6.0 (X - 3 7 2)))
(assert (= -1.0 (X + 3 7 2)))
; SEJA FUNCAO_QUADRADA, RETORNA UM MAP CONTENDO CALCULO DE DELTA, X1, X2
(defn FUNCAO_QUADRADA [A B C]
  (let [
      X1 (X - A B C)
      X2 (X + A B C)
      ]
   {:X1 X1 :X2 X2 :DELTA (DELTA A B C)}
  )
)
; SEJA DEF ASSERT_FUNCAO_QUADRADA
(def ASSERT_FUNCAO_QUADRADA {:X1 -6.0 :X2 -1.0 :DELTA 25.0})
(assert (= ASSERT_FUNCAO_QUADRADA (FUNCAO_QUADRADA 3 7 2)))
;OU DE FORMA MAIS SIMPLES, SEJA FUNCAO_SEGUNDO GRAU
;UM MAPA QUE RETORNA DELTA, X1, X2
(def FUNCAO_SEGUNDO_GRAU
  #(
    { :X1  (X - %1 %2 %3)
      :X2  (X - %1 %2 %3)
      :DELTA (DELTA %1 %2 %3)
    }
  )
)
(assert (= ASSERT_FUNCAO_QUADRADA (FUNCAO_QUADRADA 3 7 2)))
