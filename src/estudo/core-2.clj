(ns estudo.core2)
(def w
  (fn [x] "Hello World")
)
(println "resultado é : " (w 3));

; RESUMINDO
; (f a b c)
  ; f representa uma identificação de função, seja palavra reservada da linguagem
  ; ou seja uma função definida pelo desenvolvedor
  ; a b c representam os argumentos que são passados para a função f
; LOGO CLOJURE em sua syntax trabalha apenas com lista, simbolos ou referencia de função  e valores
; na forma semantica temos lista representada por invocação, simbolos representados por uma função e valores por argumentos
;

; AGORA VAMOS TRABALHAR COM ALGUMAS CONSTRUÇÕES MAIS ELABORADAS
(map inc (list 1 2 3)) ; incremento a lista
(set #{1 2 3})


; concatenando string
(defn concatenacao [nome] (str "Olá, " nome))
(println (concatenacao "Jefferson"))
;
(def funcao (fn [nome] nome))
(println (funcao "Jefferson"))
;
(def funcao (fn [nome] (str "Função anonima chamada por : ", nome)))
(println (funcao "Jefferson"));

;Funções podem ser definidas para pegar diferentes numeros de parametros [ diferentes arity]
; arity esta associado na matematica com aridade. Numero de argumentos ou operandos tomados
(defn pessoa
([] (pessoa "Pessoa foi criada com sucesso"))
( [mensagem] (str mensagem)))
(println (pessoa "DONA FLORINDA"))


; trabalhando com variadic. variável numero de argumentos
; parece muito com ...args do java , C, C++
(defn argumentos [a & b] (str a b))
(println (argumentos "Senhor Barriga", "Seu Madruga" "Professor Girafales"))

; invocando função anonima diretamenta
; (funcao argumento)
((fn [nome email] (println "nome da pessoa = " nome " com email = " email)) "Jefferson" "lisboa")
