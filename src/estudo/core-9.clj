(ns estudo.core-9)
;TRABALHANDO COM MAP
;Maps are commonly used for two purposes -;
;to manage an association of keys to values and to represent domain application data.
;AQUI COMEÇA A BRINCADIERA FICAR MAIS SÉRIA
(def bomba {
  :embreagem (fn [] 1)
  :mostrador (fn [] 2)
  :cara-palida 183273182731281237
})
(assert (= 183273182731281237 (get bomba :cara-palida)))


; SEJA UMA FUNÇÃO QUE CALCULA sin²x + cos²x
; prove que ela é igual 1
(defn pow [x] (Math/pow x 2))
(defn funcao [x]

  (let [
    a (pow (Math/sin x))
    b (pow (Math/cos x))]
    (+ a b)
    )
)
(assert (= 1.0 (funcao Math/PI)))

; Provando que (x + y)^2 = x^2 + 2xy + y^2
(defn mathSimplePow [x] (* x x))
(defn funcao2 [x y] (+ (mathSimplePow x) (* 2 x y) (mathSimplePow y)))
(assert (= 16 (funcao2 2 2)))

; Provando que (x + y)^2 != x^2 + y^2
(defn funcao3 [x y] (+ (mathSimplePow x) (mathSimplePow y)))
(assert (not= 16 (funcao3 2 2)));

; SEJA F UMA FUNÇÃO : F(X1...XN) = Produtoria <Xi>, i = 1 até i = n
(defn F [ & x] (apply * x))
;SEJA G UMA FUNÇÃO : H(A,B) = F(A,A) + F(A,B,C) + F(B,B), PARA C = 2
(defn G [a b]
  (let [x (F a a) y (F a b 2) z (F b b)] (+ x y z))
)
(assert (= 16 (G 2 2)))

(defn W [a b]
  (let [x (F a a) y (F b b)] ( + x y))
)
(assert (not= 16 (W 2 2)))
