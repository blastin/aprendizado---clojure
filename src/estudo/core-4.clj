(ns estudo.core-4)
(def x (vector 1 2 3 4 5 6))
(defn f [& valores] (map dec valores))
(apply f 9 x)
;
; EXERCICIOS
(defn greet [] (println "Hello"))
(greet)
; UTILIZANDO DEF
(def greet
  (fn [] (println "Hello com função anonima"))
)
(greet)
; UTILIZAND DEF COM FUNÇÃO ANONIMA syntax
(def greet
#(println "Hello World com função anonima com syntax"))
(greet)
;
; Define a function greeting which:
; Given no arguments, returns "Hello, World!"
;
; Given one argument x, returns "Hello, x!"
;
; Given two arguments x and y, returns "x, y!"
;
(defn greeting
([]    (greeting "Hello" "World"))
([x]   (greeting "Hello" x))
([x y] (str x ", " y "!")))

(println (greeting))
(println (greeting "oi"))
(println (greeting "olá" "mundo"))

(assert (= "Hello, World!" (greeting)))
(assert (= "Hello, Clojure!" (greeting "Clojure")))
(assert (= "Good morning, Clojure!" (greeting "Good morning" "Clojure")))

; função f(x) = x
(defn funcao [x] x)
(println (funcao "Matrix 4"))
(assert (= "Matrix 4" (funcao "Matrix 4")))
;Define a function always-thing which takes any number of arguments,
; ignores all of them, and returns the number 100.
(defn always-thing  [& argumento] 100)
(assert (= 100 (apply always-thing '(1 2 3 4 5 6 7))))
;

;Define a function make-thingy which takes a single argument x.
;It should return another function,
; which takes any number of arguments and always returns x.
(defn  make-thingy [x] (fn [& argumento] x))
(assert (= 100 ((make-thingy 100) 5 6 7 8 9 10 11 12 13)))
